﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;
using Trello_DocumentGeneration.Services;
using Trello_DocumentGeneration.Services.GoogleAPI;
using Trello_DocumentGeneration.Services.GoogleAPI.TemplatesInfo.Templates;
using Trello_DocumentGeneration.Services.Trello;

namespace Trello_DocumentGeneration.Controllers
{
    [Route("documents")]
    [ApiController]
    public class DocumentGeneratorController : ControllerBase
    {
        private const String DRIVE_FOLDER_ID = "0B0tzdLOAnsR_SG5nNWgwR2pjRWs";
        private const String SPREADSHEET_ID = "1ro2twFJDnhMOO9EIpHfT0L9-JyiKOHbV7IMAPg9e08s";
        private readonly GoogleAPIService _googleService;
        private readonly WhiteCubeLastIndexes _lastIndexes;
        private readonly TrelloService _trelloService;
        public DocumentGeneratorController(GoogleAPIService gService, WhiteCubeLastIndexes lastIndexes, TrelloService trelloService)
        {
            _trelloService = trelloService;
            _googleService = gService;
            _lastIndexes = lastIndexes;
        }
        [HttpPost("applyworker")]
        public async Task<IActionResult> GenerateDocumentAsync([FromBody] CardIdModel cardIdModel)
        {
            bool updateFolderId = false;
            Employee employee = _trelloService.GetEmployee(cardIdModel.CardId).CreateWorkIndexes(_lastIndexes);
            IDocumentTemplateFiller contract;
            IDocumentTemplateFiller applicationOrderTemplate = new ApplicationOrderTemplate(employee);
            switch (employee.CommunicationType)
            {
                case "ГПХ":
                    contract = new PaidServicesAgreementTemplate(employee);
                    break;
                case "ТРУДОВОЙ ДОГОВОР":
                    contract = new EmploymentContractTemplate(employee);
                    break;
                default:
                    return BadRequest();
            }
            if (string.IsNullOrEmpty(employee.GoogleDriveFolderId))
            {
                employee.GoogleDriveFolderId = await _googleService.CreateFolderAsync(new DriveSettings(DRIVE_FOLDER_ID), applicationOrderTemplate.GetFolderName());
                updateFolderId = true;
            }
            await _googleService.GenerateDocumentAsync(applicationOrderTemplate, employee.GoogleDriveFolderId);
            await _googleService.GenerateDocumentAsync(contract, employee.GoogleDriveFolderId);
            UpdateEmployee(employee, updateFolderId);
            await _googleService.AddRowToSpreadSheet(employee, SPREADSHEET_ID);
            return Ok();
        }
        [HttpPost("fireworker")]
        public async Task<IActionResult> FireWorkerAsync([FromBody] CardIdModel cardIdModel)
        {
            Employee employee = _trelloService.GetEmployee(cardIdModel.CardId).CreateFireIndex(_lastIndexes);
            IDocumentTemplateFiller fireOrder = new FireOrderTemplate(employee);
            await _googleService.GenerateDocumentAsync(fireOrder, employee.GoogleDriveFolderId);
            CreateFireInfo(employee);
            return Ok();
        }
        private void UpdateEmployee(Employee employee, bool updateFolderId = false)
        {
            _trelloService.UpdateContractNumber(employee.ContractNumber, employee.CardId);
            _trelloService.UpdateOrderInfo(employee.OrderInfo, employee.CardId);
            _trelloService.UpdateApplicationDate(employee.ApplicationDate.GetValueOrDefault(), employee.CardId);
            if (updateFolderId)
            {
                _trelloService.UpdateGoogleFolderId(employee.GoogleDriveFolderId, employee.CardId);
            }
        }
        private void CreateFireInfo(Employee employee)
        {
            _trelloService.UpdateFireOrderIndex(employee.FireInfo, employee.CardId);
            _trelloService.UpdateFireOrderDate(employee.FireDate.GetValueOrDefault(), employee.CardId);
        }

    }
}
