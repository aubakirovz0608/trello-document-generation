﻿using Newtonsoft.Json;

namespace Trello_DocumentGeneration.Models
{
    public class CardIdModel
    {
        [JsonProperty("cardId")]
        public string CardId { get; set; }
    }
}
