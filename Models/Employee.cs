﻿using Newtonsoft.Json;
using System;
using Trello_DocumentGeneration.Services;
using Trello_DocumentGeneration.Services.Trello.Attributes;

namespace Trello_DocumentGeneration.Models
{
    public class Employee
    {
        [JsonProperty("CardId")]
        public string CardId { get; set; }


        [TrelloProperty("5be29c15390afe25df68a6b0")]
        [JsonProperty("Name")]
        public string Name { get; set; }


        [TrelloProperty("5be29c00578cd10db9426f36")]
        [JsonProperty("Surname")]
        public string Surname { get; set; }


        [TrelloProperty("5be29c255b65f282bb8192ee")]
        [JsonProperty("Patronymic")]
        public string Patronymic { get; set; }


        [TrelloProperty("5aec24816c8fb4461419cb17")]
        [JsonProperty("IIN")]
        public string IIN { get; set; }


        [TrelloProperty("5beacade61b1078e5d1bc939")]
        [JsonProperty("PassportInfo")]
        public string PassportInfo { get; set; }


        [TrelloProperty("5be2971f59c6e024edf64a20")]
        [JsonProperty("Address")]
        public string Address { get; set; }


        [TrelloProperty("5f3194b07b3033450a8aec56")]
        [JsonProperty("HomeAddress")]
        public string HomeAddress { get; set; }


        [TrelloProperty("5aec1e41020246c271489473")]
        [JsonProperty("PhoneNumber")]
        public string PhoneNumber { get; set; }


        [TrelloProperty("5b02883649ea5e59b3774cfb")]
        [JsonProperty("CorporativePhoneNumber")]
        public string CorporativePhoneNumber { get; set; }


        [TrelloProperty("5be297c8247cbc4705099713")]
        [JsonProperty("PhoneTariffPlan")]
        public string PhoneTariffPlan { get; set; }


        [TrelloProperty("5b028b3beb918183242d3004")]
        [JsonProperty("Tonix")]
        public string Tonix { get; set; }


        [TrelloProperty("5b028dbeddd6d485414db55b")]
        [JsonProperty("Email")]
        public string Email { get; set; }


        [TrelloProperty("5be01230c60d32532dca838b")]
        [JsonProperty("EmailPassword")]
        public string EmailPassword { get; set; }


        [TrelloProperty("5be29d4fac64667d2912ec1b")]
        [JsonProperty("GoogleAccountLogin")]
        public string GoogleAccountLogin { get; set; }


        [TrelloProperty("5be0129593a9254ea098858f")]
        [JsonProperty("GoogleAccountPassword")]
        public string GoogleAccountPassword { get; set; }


        [TrelloProperty("5aec1de803119846a7593640")]
        [JsonProperty("ApplicationDate")]
        public DateTime? ApplicationDate { get; set; }


        [TrelloProperty("5aec1e4bf8b5ec2ae70508fd")]
        [JsonProperty("BirthDate")]
        public DateTime? BirthDate { get; set; }


        [TrelloProperty("5e255e5305c79a575b57a2a7")]
        [JsonProperty("Company")]
        public string Company { get; set; }


        [TrelloProperty("5be2879b4e54812cabd02a1f")]
        [JsonProperty("Position")]
        public string Position { get; set; }


        [TrelloProperty("5beaca80236f9e276f1fc4cf")]
        [JsonProperty("CommunicationType")]
        public string CommunicationType { get; set; }


        [TrelloProperty("5f1ff16bbd591d1395819196")]
        [JsonProperty("Salary")]
        public string Salary { get; set; }


        [TrelloProperty("5aec249f79009cbb0355f5ef")]
        [JsonProperty("CardNumber")]
        public string CardNumber { get; set; }


        [TrelloProperty("5aec1f6d7a4aa9c1f2151aae")]
        [JsonProperty("ContractNumber")]
        public string ContractNumber { get; set; }


        [TrelloProperty("5aec1f9a458205c217b15839")]
        [JsonProperty("OrderInfo")]
        public string OrderInfo { get; set; }


        [TrelloProperty("5aec1fae5b0d845b1b5b0e92")]
        [JsonProperty("FireInfo")]
        public string FireInfo { get; set; }


        [JsonProperty("WorkIndex")]
        public int WorkIndex { get; set; }


        [JsonProperty("OrderIndex")]
        public int OrderIndex { get; set; }


        [JsonProperty("FireIndex")]
        public int FireIndex { get; set; }


        [TrelloProperty("5aec1df98084278b251a9370")]
        [JsonProperty("FireDate")]
        public DateTime? FireDate { get; set; }


        [TrelloProperty("5c08d1df31c402803f1c4567")]
        [JsonProperty("GoogleDriveFolderId")]
        public string GoogleDriveFolderId { get; set; }


        public Employee CreateWorkIndexes(WhiteCubeLastIndexes lastIndexes)
        {
            switch (this.CommunicationType)
            {
                case "ГПХ":
                    WorkIndex = new Random().Next() % 90000 + 10000;
                    ContractNumber = $"{WorkIndex}";
                    switch (this.Company)
                    {
                        case @"ИП ""Белый КУБ""":
                            OrderIndex = ++lastIndexes.IPIndexes.ApplyOrderLastIndex;
                            break;
                        case @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""":
                            OrderIndex = ++lastIndexes.ProIndexes.ApplyOrderLastIndex;
                            break;
                        case @"ТОО ""Мебельное ателье Белый КУБ""":
                            OrderIndex = ++lastIndexes.TOOIndexes.ApplyOrderLastIndex;
                            break;

                    }
                    break;
                case "ТРУДОВОЙ ДОГОВОР":
                    switch (this.Company)
                    {
                        case @"ИП ""Белый КУБ""":
                            WorkIndex = ++lastIndexes.IPIndexes.EmploymentContractLastIndex;
                            OrderIndex = ++lastIndexes.IPIndexes.ApplyOrderLastIndex;
                            break;
                        case @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""":
                            WorkIndex = ++lastIndexes.ProIndexes.EmploymentContractLastIndex;
                            OrderIndex = ++lastIndexes.ProIndexes.ApplyOrderLastIndex;
                            break;
                        case @"ТОО ""Мебельное ателье Белый КУБ""":
                            WorkIndex = ++lastIndexes.ProIndexes.EmploymentContractLastIndex;
                            OrderIndex = ++lastIndexes.TOOIndexes.ApplyOrderLastIndex;
                            break;
                    }
                    ContractNumber = $"{WorkIndex}/{DateTime.Now:MM-yyyy}";
                    break;
            }
            lastIndexes.SaveCurrentState().Wait();
            OrderInfo = $"HR-05-ПР-{OrderIndex}/{DateTime.Now:MM.yyyy}";
            ApplicationDate = DateTime.Now;
            return this;

        }
        public Employee CreateFireIndex(WhiteCubeLastIndexes lastIndexes)
        {
            switch (this.CommunicationType)
            {
                case "ГПХ":
                    switch (this.Company)
                    {
                        case @"ИП ""Белый КУБ""":
                            FireIndex = ++lastIndexes.IPIndexes.FireOrderLastIndex;
                            break;
                        case @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""":
                            FireIndex = ++lastIndexes.ProIndexes.FireOrderLastIndex;
                            break;
                        case @"ТОО ""Мебельное ателье Белый КУБ""":
                            FireIndex = ++lastIndexes.TOOIndexes.FireOrderLastIndex;
                            break;

                    }
                    break;
                case "ТРУДОВОЙ ДОГОВОР":
                    switch (this.Company)
                    {
                        case @"ИП ""Белый КУБ""":
                            FireIndex = ++lastIndexes.IPIndexes.FireOrderLastIndex;
                            break;
                        case @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""":
                            FireIndex = ++lastIndexes.ProIndexes.FireOrderLastIndex;
                            break;
                        case @"ТОО ""Мебельное ателье Белый КУБ""":
                            FireIndex = ++lastIndexes.TOOIndexes.FireOrderLastIndex;
                            break;
                    }
                    break;
            }
            FireInfo = $"HR-05-ПУ-{FireIndex}/{DateTime.Now:MM.yyyy}";
            lastIndexes.SaveCurrentState().Wait();
            FireDate = DateTime.Now;
            return this;

        }
    }
}
