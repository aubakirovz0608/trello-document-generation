﻿namespace Trello_DocumentGeneration.Services
{
    public class LastIndexesInfo
    {
        public int EmploymentContractLastIndex { get; set; }
        public int ApplyOrderLastIndex { get; set; }
        public int FireOrderLastIndex { get; set; }

    }
}