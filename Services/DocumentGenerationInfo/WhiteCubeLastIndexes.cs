﻿using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace Trello_DocumentGeneration.Services
{
    public class WhiteCubeLastIndexes
    {
        private string FileName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public LastIndexesInfo IPIndexes { get; set; }
        public LastIndexesInfo TOOIndexes { get; set; }
        public LastIndexesInfo ProIndexes { get; set; }
        public WhiteCubeLastIndexes()
        {

        }
        public WhiteCubeLastIndexes(string fileName)
        {
            FileName = fileName;
            using FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate);
            WhiteCubeLastIndexes lastIndexes = JsonSerializer.DeserializeAsync<WhiteCubeLastIndexes>(fileStream).Result;
            Month = lastIndexes.Month;
            Year = lastIndexes.Year;
            if (Year == DateTime.Now.AddHours(6).Year)
            {
                IPIndexes = lastIndexes.IPIndexes;
                TOOIndexes = lastIndexes.TOOIndexes;
                ProIndexes = lastIndexes.ProIndexes;
            }
            else
            {
                fileStream.Close();
                Month = DateTime.Now.AddHours(6).Month;
                Year = DateTime.Now.AddHours(6).Year;
                IPIndexes = new LastIndexesInfo();
                TOOIndexes = new LastIndexesInfo();
                ProIndexes = new LastIndexesInfo();
                SaveCurrentState().Wait();
            }
        }
        public async Task SaveCurrentState()
        {
            using FileStream fileStream = new FileStream(FileName, FileMode.Create);
            await JsonSerializer.SerializeAsync(fileStream, this);
        }
    }
}
