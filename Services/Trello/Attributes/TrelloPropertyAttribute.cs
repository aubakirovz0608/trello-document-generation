﻿using System;

namespace Trello_DocumentGeneration.Services.Trello.Attributes
{
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class TrelloPropertyAttribute : Attribute
    {
        readonly string _trelloPropertyId;

        public TrelloPropertyAttribute(string trelloPropertyId)
        {
            this._trelloPropertyId = trelloPropertyId;
        }

        public string TrelloPropertyId
        {
            get { return _trelloPropertyId; }
        }
    }
}
