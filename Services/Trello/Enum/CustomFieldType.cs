﻿using System.Runtime.Serialization;

namespace Trello_DocumentGeneration.Services.Trello.Enum
{
    public enum CustomFieldType
    {
        [EnumMember(Value = "text")]
        TEXT,
        [EnumMember(Value = "number")]
        NUMBER,
        [EnumMember(Value = "list")]
        LIST,
        [EnumMember(Value = "date")]
        DATE,
        [EnumMember(Value = "checkbox")]
        CHECKBOX
    }
}
