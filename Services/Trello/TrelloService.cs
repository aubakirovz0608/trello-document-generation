﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Trello_DocumentGeneration.Models;
using Trello_DocumentGeneration.Services.Trello.Attributes;
using Trello_DocumentGeneration.Services.Trello.Poco;

namespace Trello_DocumentGeneration.Services.Trello
{
    public class TrelloService
    {
        public string Key { get; set; }
        public string Token { get; set; }
        public string BoardId { get; set; }
        public string OrderInfoFieldId { get; set; }
        public string ContractNumberFieldId { get; set; }
        public string ApplicationDateFieldId { get; set; }
        public string FireOrderIndexFieldId { get; set; }
        public string FireOrderDateFieldId { get; set; }
        public string GoogleDriveFolderIdFieldId { get; set; }

        private readonly RestClient TrelloAPIClient = new RestClient("https://api.trello.com");
        public TrelloService()
        {

        }
        public TrelloService(string key, string token)
        {
            Key = key;
            Token = token;
        }
        public static TrelloService GetService(string credentialsPath)
        {
            using FileStream fileStream = new FileStream(credentialsPath, FileMode.Open);
            return System.Text.Json.JsonSerializer.DeserializeAsync<TrelloService>(fileStream).Result;
        }
        public void UpdateOrderInfo(string orderInfo, string cardId)
        {
            GenerateFieldUpdateRequest(OrderInfoFieldId, cardId, orderInfo);
        }
        public void UpdateContractNumber(string contractNumber, string cardId)
        {
            GenerateFieldUpdateRequest(ContractNumberFieldId, cardId, contractNumber);
        }
        public void UpdateApplicationDate(DateTime date, string cardId)
        {
            GenerateFieldUpdateRequest(ApplicationDateFieldId, cardId, date);
        }
        public void UpdateFireOrderIndex(string fireOrderIndex, string cardId)
        {
            GenerateFieldUpdateRequest(FireOrderIndexFieldId, cardId, fireOrderIndex);
        }
        public void UpdateFireOrderDate(DateTime fireOrderDate, string cardId)
        {
            GenerateFieldUpdateRequest(FireOrderDateFieldId, cardId, fireOrderDate);
        }
        public void UpdateGoogleFolderId(string folderId, string cardId)
        {
            GenerateFieldUpdateRequest(GoogleDriveFolderIdFieldId, cardId, folderId);
        }
        public Employee GetEmployee(string cardId)
        {
            Card trelloCard = GetCard(cardId);
            IEnumerable<CustomField> boardCustomFields = GetBoardCustomFields(BoardId);
            return GetEmployee(cardId, trelloCard, boardCustomFields);
        }
        private Employee GetEmployee(string shortCardId, Card card, IEnumerable<CustomField> boardCustomFields)
        {
            Employee employee = new Employee
            {
                CardId = shortCardId
            };
            PropertyInfo[] properties = typeof(Employee).GetProperties();
            foreach (var property in properties)
            {
                Type trelloCustomFieldAttributeType = typeof(TrelloPropertyAttribute);
                if (Attribute.IsDefined(property, trelloCustomFieldAttributeType))
                {
                    TrelloPropertyAttribute trelloPropertyAttribute = (TrelloPropertyAttribute)Attribute.GetCustomAttribute(property, trelloCustomFieldAttributeType);
                    string trelloPropertyId = trelloPropertyAttribute.TrelloPropertyId;
                    CustomField customField = boardCustomFields.Where(boardCustomField => boardCustomField.Id == trelloPropertyId).First();
                    CustomFieldItem customFieldItem = card.CustomFieldItems.Where(customFieldItem => customFieldItem.IdCustomField == trelloPropertyId).FirstOrDefault();
                    if (customFieldItem != null)
                    {
                        object value;
                        switch (customField.Type)
                        {
                            case Enum.CustomFieldType.TEXT:
                                value = customFieldItem.Value.Text;
                                break;
                            case Enum.CustomFieldType.NUMBER:
                                value = customFieldItem.Value.Number;
                                break;
                            case Enum.CustomFieldType.LIST:
                                Option option = customField.Options.Where(option => customFieldItem.IdValue == option.Id).First();
                                value = option.Value.Text;
                                break;
                            case Enum.CustomFieldType.DATE:
                                value = customFieldItem.Value.Date;
                                break;
                            case Enum.CustomFieldType.CHECKBOX:
                                value = customFieldItem.Value.Checked;
                                break;
                            default:
                                throw new Exception("Custom field type cannot be null");
                        }
                        property.SetValue(employee, value);
                    }
                    else
                    {
                        property.SetValue(employee, null);
                    }
                }
            }
            return employee;
        }
        private Card GetCard(string cardId)
        {
            return JsonConvert.DeserializeObject<Card>(GenerateCardRequest(cardId).Content);
        }
        private IEnumerable<CustomField> GetBoardCustomFields(string boardId)
        {
            return JsonConvert.DeserializeObject<IEnumerable<CustomField>>(GenerateBoardRequest(boardId).Content);
        }
        private IRestResponse GenerateBoardRequest(string boardId)
        {
            RestRequest boardRequest = new RestRequest($"1/boards/{boardId}/customFields");
            AddDefaultRestRequestParameters(boardRequest);
            var response = TrelloAPIClient.Get(boardRequest);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"Board REST request failed. Message: {response.Content}.");
            }
            return response;
        }
        private IRestResponse GenerateCardRequest(string cardId)
        {
            RestRequest cardRequest = new RestRequest($"/1/cards/{cardId}/");
            AddDefaultRestRequestParameters(cardRequest);
            cardRequest.AddQueryParameter("fields", "name");
            cardRequest.AddQueryParameter("customFieldItems", "true");
            var response = TrelloAPIClient.Get(cardRequest);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"Card REST request failed. Message: {response.Content}.");
            }
            return response;

        }
        private IRestResponse GenerateFieldUpdateRequest(string fieldId, string cardId, string value)
        {
            CustomFieldValue customFieldValue = new CustomFieldValue() { Text = value };
            return GenerateFieldUpdateRequest(fieldId, cardId, customFieldValue);
        }
        private IRestResponse GenerateFieldUpdateRequest(string fieldId, string cardId, DateTime value)
        {
            CustomFieldValue customFieldValue = new CustomFieldValue() { Date = value };
            return GenerateFieldUpdateRequest(fieldId, cardId, customFieldValue);
        }

        private IRestResponse GenerateFieldUpdateRequest(string fieldId, string cardId, CustomFieldValue value)
        {
            RestRequest updateRequest = new RestRequest($"/1/cards/{cardId}/customField/{fieldId}/item");
            CustomFieldUpdateRequest customFieldUpdateRequestBody = new CustomFieldUpdateRequest() { CustomFieldValue = value };
            string customFieldUpdateRequestBodyJSON = JsonConvert.SerializeObject(customFieldUpdateRequestBody);

            AddDefaultRestRequestParameters(updateRequest);
            updateRequest.AddJsonBody(customFieldUpdateRequestBodyJSON);
            var response = TrelloAPIClient.Put<RestResponse>(updateRequest);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(customFieldUpdateRequestBodyJSON);
            }
            return response;
        }
        private void AddDefaultRestRequestParameters(RestRequest restRequest)
        {
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddQueryParameter("key", Key);
            restRequest.AddQueryParameter("token", Token);
        }
    }
}
