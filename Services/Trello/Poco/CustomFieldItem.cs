﻿using Newtonsoft.Json;

namespace Trello_DocumentGeneration.Services.Trello.Poco
{
    public class CustomFieldItem
    {
        [JsonProperty("value")]
        public CustomFieldValue Value { get; set; }

        [JsonProperty("idCustomField")]
        public string IdCustomField { get; set; }

        [JsonProperty("idValue")]
        public string IdValue { get; set; }
    }
}
