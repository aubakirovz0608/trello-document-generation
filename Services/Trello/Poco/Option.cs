﻿using Newtonsoft.Json;

namespace Trello_DocumentGeneration.Services.Trello.Poco
{
    public class Option
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("value")]
        public CustomFieldValue Value { get; set; }
    }
}
