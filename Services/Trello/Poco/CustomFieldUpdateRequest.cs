﻿using Newtonsoft.Json;
using Trello_DocumentGeneration.Services.Trello.Poco;

namespace Trello_DocumentGeneration.Services.Trello
{
    public class CustomFieldUpdateRequest
    {
        [JsonProperty("value")]
        public CustomFieldValue CustomFieldValue { get; set; }
    }
}
