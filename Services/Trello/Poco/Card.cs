﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Trello_DocumentGeneration.Services.Trello.Poco
{
    public class Card
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("customFieldItems")]
        public List<CustomFieldItem> CustomFieldItems { get; set; }
    }
}
