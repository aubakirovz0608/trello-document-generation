﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Trello_DocumentGeneration.Services.Trello.Enum;

namespace Trello_DocumentGeneration.Services.Trello.Poco
{
    public class CustomField
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CustomFieldType Type { get; set; }

        [JsonProperty("options")]
        public List<Option> Options { get; set; }
    }
}
