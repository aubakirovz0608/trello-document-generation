﻿using Newtonsoft.Json;
using System;

namespace Trello_DocumentGeneration.Services.Trello.Poco
{
    public class CustomFieldValue
    {
        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Date { get; set; }

        [JsonProperty("checked", NullValueHandling = NullValueHandling.Ignore)]
        public string Checked { get; set; }

        [JsonProperty("number", NullValueHandling = NullValueHandling.Ignore)]
        public string Number { get; set; }
    }
}
