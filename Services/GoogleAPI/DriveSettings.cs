﻿namespace Trello_DocumentGeneration.Services.GoogleAPI
{
    public class DriveSettings
    {
        public string SaveFolderId { get; set; }
        public DriveSettings(string saveFolderId)
        {
            SaveFolderId = saveFolderId;
        }
    }
}
