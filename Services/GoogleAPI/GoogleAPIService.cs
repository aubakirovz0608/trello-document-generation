﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Docs.v1;
using Google.Apis.Docs.v1.Data;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;
using static Google.Apis.Sheets.v4.SpreadsheetsResource.ValuesResource;
using static Google.Apis.Sheets.v4.SpreadsheetsResource.ValuesResource.AppendRequest;
using static Google.Apis.Sheets.v4.SpreadsheetsResource.ValuesResource.BatchGetRequest;

namespace Trello_DocumentGeneration.Services.GoogleAPI
{
    public class GoogleAPIService
    {
        private string CredentialsPath { get; set; }
        private GoogleCredential Credentials { get; set; }
        private string ApplicationName { get; set; }
        public DocsService Docs { get; set; }
        public DriveService Drive { get; set; }
        public SheetsService Sheets { get; set; }
        public GoogleAPIService(string credentialsPath, IEnumerable<string> scopes, string applicationName)
        {
            this.ApplicationName = applicationName;
            this.CredentialsPath = credentialsPath;
            using (var stream = new System.IO.FileStream(this.CredentialsPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                Credentials = GoogleCredential.FromStream(stream)
                    .CreateScoped(scopes);
            }

            Docs = new DocsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = this.Credentials,
                ApplicationName = this.ApplicationName
            });
            Drive = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = this.Credentials,
                ApplicationName = this.ApplicationName,
            });
            Sheets = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = this.Credentials,
                ApplicationName = this.ApplicationName
            });
        }
        public async Task<BatchUpdateDocumentResponse> GenerateDocumentAsync(IDocumentTemplateFiller templateSettings, string folderId = default)
        {
            string copyId = await CopyDocument(templateSettings.GetTemplateId(), folderId, templateSettings.GetFileName()); // Сделать копию документа
            BatchUpdateDocumentRequest templateRequest = await templateSettings.CreateFillRequest(); // Создать запрос на изменение
            return await Docs.Documents.BatchUpdate(templateRequest, copyId).ExecuteAsync(); // Выполнить запрос на изменение
        }
        public async Task<string> CopyDocument(string fileId, string saveFolderId, string fileName = null) // Возвращает ID нового документа
        {
            File copyFile = await Drive.Files.Get(fileId).ExecuteAsync();
            copyFile.Id = null;
            copyFile.Parents = new List<string>() { saveFolderId };
            if (!string.IsNullOrEmpty(fileName)) copyFile.Name = fileName;
            return (await Drive.Files.Copy(copyFile, fileId).ExecuteAsync()).Id;
        }
        public async Task<string> CreateFolderAsync(DriveSettings settings, string folderName = null) // Возвращает ID новой папки
        {
            File folder = new File() { MimeType = "application/vnd.google-apps.folder", Parents = new List<string> { settings.SaveFolderId } };
            if (!string.IsNullOrEmpty(folderName)) folder.Name = folderName;
            return (await Drive.Files.Create(folder).ExecuteAsync()).Id;
        }
        public async Task<UpdateValuesResponse> UpdateRowInSpreadsheet(Employee employee, string spreadsheetId)
        {
            const int CHAR_ALPHABET_START = 65;
            List<string> cardIds = (await GetCardIds(spreadsheetId)).ToList();
            string spreadsheetCardId = cardIds.Where(cardId => ((String)cardId) == employee.CardId).First();
            if (spreadsheetCardId != null)
            {
                int rowIndex = cardIds.IndexOf(spreadsheetCardId) + 1; //notation starts from 1
                IList<Object> data = GenerateEmployeeDataList(employee);
                string range = $"{(char)CHAR_ALPHABET_START}{rowIndex}:{(char)(CHAR_ALPHABET_START + data.Count)}{rowIndex}";

                IList<IList<object>> requestBodyValues = GenerateRequestBodySingleRowValues(data);
                UpdateRequest updateRequest = GenerateUpdateRequest(requestBodyValues, spreadsheetId, range);
                return await updateRequest.ExecuteAsync();
            }
            return null;
        }
        public async Task<IEnumerable<string>> GetCardIds(string spreadsheetId)
        {
            string cardIdRange = "A1:A10000"; //A - колонка, где находится CardId
            GetRequest getRequest = Sheets.Spreadsheets.Values.Get(spreadsheetId, cardIdRange);
            getRequest.MajorDimension = GetRequest.MajorDimensionEnum.COLUMNS;
            ValueRange responseValueRange = await getRequest.ExecuteAsync();
            IEnumerable<string> cardIds = responseValueRange.Values.First().Select(cardId => (String)cardId);
            return cardIds;
        }
        public async Task<AppendValuesResponse> AddRowToSpreadSheet(Employee employee, string spreadsheetId)
        {
            IList<Object> data = GenerateEmployeeDataList(employee);
            string range = $"A1:A{data.Count}";
            IList<IList<object>> requestBodyValues = GenerateRequestBodySingleRowValues(data);
            AppendRequest appendRequest = GenerateAppendRequest(requestBodyValues, spreadsheetId, range);
            return await appendRequest.ExecuteAsync();
        }
        public async Task<string> FindFolderId(string name)
        {
            List<File> allFilesAndFolders = new List<File>();
            FilesResource.ListRequest request = Drive.Files.List();
            do
            {
                try
                {
                    FileList files = await request.ExecuteAsync();
                    allFilesAndFolders.AddRange(files.Files);
                    request.PageToken = files.NextPageToken;
                }
                catch (Exception)
                {
                    throw;
                }
            } while (!String.IsNullOrEmpty(request.PageToken));
            return allFilesAndFolders.Where(file => file.MimeType == "application/vnd.google-apps.folder" && file.Name.Contains(name)).FirstOrDefault()?.Id;
        }
        private UpdateRequest GenerateUpdateRequest(IList<IList<Object>> data, string spreadsheetId, string range)
        {
            ValueRange requestBody = new ValueRange()
            {
                MajorDimension = Enum.GetName(typeof(MajorDimensionEnum), MajorDimensionEnum.ROWS),
                Range = range,
                Values = data
            };
            UpdateRequest updateRequest = Sheets.Spreadsheets.Values.Update(requestBody, spreadsheetId, range);
            updateRequest.ValueInputOption = UpdateRequest.ValueInputOptionEnum.USERENTERED;
            return updateRequest;
        }
        private AppendRequest GenerateAppendRequest(IList<IList<object>> data, string spreadsheetId, string range)
        {
            ValueRange requestBody = new ValueRange()
            {
                MajorDimension = Enum.GetName(typeof(MajorDimensionEnum), MajorDimensionEnum.ROWS),
                Range = range,
                Values = data
            };
            AppendRequest appendRequest = Sheets.Spreadsheets.Values.Append(requestBody, spreadsheetId, range);
            appendRequest.InsertDataOption = InsertDataOptionEnum.INSERTROWS;
            appendRequest.ValueInputOption = AppendRequest.ValueInputOptionEnum.USERENTERED;
            return appendRequest;
        }
        private IList<IList<Object>> GenerateRequestBodySingleRowValues(IList<Object> data)
        {
            return new List<IList<Object>>()
            {
                data
            };
        }
        private IList<Object> GenerateEmployeeDataList(Employee employee)
        {
            return new List<Object>()
            {
                employee.CardId,
                employee.Name,
                employee.Surname,
                employee.Patronymic,
                employee.Address,
                employee.BirthDate == null ? null : employee.BirthDate?.ToString("MM/dd/yyyy"),
                employee.IIN,
                employee.PassportInfo,
                employee.Position,
                employee.Company,
                employee.CorporativePhoneNumber,
                employee.PhoneNumber,
                employee.CardNumber,
                employee.ApplicationDate == null ? null : employee.ApplicationDate?.ToString("MM/dd/yyyy HH:mm"),
                employee.OrderInfo,
                employee.ContractNumber,
                employee.CommunicationType,
                employee.PhoneTariffPlan,
                employee.Tonix == "checked" ? "Да" : "Нет",
                employee.Email,
                employee.EmailPassword,
                employee.GoogleAccountLogin,
                employee.GoogleAccountPassword,
                employee.FireInfo,
                employee.FireDate == null ? null : employee.FireDate?.ToString("MM/dd/yyyy HH:mm"),
            };
        }
    }
}
