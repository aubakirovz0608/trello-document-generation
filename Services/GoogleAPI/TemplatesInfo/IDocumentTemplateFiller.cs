﻿using Google.Apis.Docs.v1.Data;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;

namespace Trello_DocumentGeneration.Services.GoogleAPI
{
    public interface IDocumentTemplateFiller
    {
        public Employee Employee { get; set; }
        public Task<BatchUpdateDocumentRequest> CreateFillRequest();
        public string GetFileName();
        public string GetTemplateId();
        public string GetFolderName();
    }
}
