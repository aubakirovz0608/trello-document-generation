﻿using Google.Apis.Docs.v1.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;

namespace Trello_DocumentGeneration.Services.GoogleAPI.TemplatesInfo.Templates
{
    public class EmploymentContractTemplate : IDocumentTemplateFiller
    {
        public Employee Employee { get; set; }
        public EmploymentContractTemplate(Employee employee)
        {
            Employee = employee;
        }
        public async Task<BatchUpdateDocumentRequest> CreateFillRequest()
        {
            Declensions fullNameDeclensions = await Declensions.GetDeclensions(new List<string> { Employee.Surname, Employee.Name, Employee.Patronymic });
            List<Request> requests = new List<Request>()
            {
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Surname}}", MatchCase = true }, ReplaceText = (await Declensions.GetDeclensions(new List<string> { Employee.Surname })).Prepositional }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullName}}", MatchCase = true }, ReplaceText = $"{Employee.Surname} {Employee.Name} {Employee.Patronymic}" }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Position}}", MatchCase = true }, ReplaceText = Employee.Position }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{IIN}}", MatchCase = true }, ReplaceText = Employee.IIN }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Date}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{NextYearDate}}", MatchCase = true }, ReplaceText = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Month}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("MM") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Year}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("yy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{WorkIndex}}", MatchCase = true }, ReplaceText = Employee.WorkIndex.ToString() }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{PassportInfo}}", MatchCase = true }, ReplaceText = Employee.PassportInfo }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullNameTo}}", MatchCase = true }, ReplaceText = fullNameDeclensions.Dative} },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Address}}", MatchCase = true }, ReplaceText = Employee.Address.Replace("Адрес прописки:", "" )} },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{HomeAddress}}", MatchCase = true }, ReplaceText = Employee.HomeAddress } },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{PhoneNumber}}", MatchCase = true }, ReplaceText = Employee.PhoneNumber } },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Salary}}", MatchCase = true }, ReplaceText = Employee.Salary +  $"({(await Declensions.GetWordsFromNumber(Int32.Parse(string.IsNullOrEmpty(Employee.Salary) ? "0" : Employee.Salary))).Numbers.Nominative})" } }
            };
            return new BatchUpdateDocumentRequest() { Requests = requests };
        }

        public string GetFileName()
        {
            return $"Трудовой договор {Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetFolderName()
        {
            return $"{Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetTemplateId()
        {
            if (Employee.Company == @"ИП ""Белый КУБ""") return "1y8wVYPgTDpLfrVR1xTxLBUaRxgVEfUSbdoiNhrmkj0c";
            else if (Employee.Company == @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""") return "1MEAPhwRvYey0MlGcBpeqxKdZI9d68X4DqVRnbdM2kb4";
            else if (Employee.Company == @"ТОО ""Мебельное ателье Белый КУБ""") return "1nzQTQ0B9orfcQXMvb3Sx91IBwZQh_I2wyXI9PaEe5t4";
            else throw new ArgumentException();
        }
    }
}
