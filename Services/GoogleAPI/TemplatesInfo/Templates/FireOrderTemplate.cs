﻿using Google.Apis.Docs.v1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;

namespace Trello_DocumentGeneration.Services.GoogleAPI.TemplatesInfo.Templates
{
    public class FireOrderTemplate : IDocumentTemplateFiller
    {
        public Employee Employee { get; set; }
        public FireOrderTemplate(Employee employee)
        {
            Employee = employee;
        }
        public async Task<BatchUpdateDocumentRequest> CreateFillRequest()
        {
            List<Request> fillRequests = new List<Request>()
            {
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Surname}}", MatchCase = true }, ReplaceText = (await Declensions.GetDeclensions(new List<string> { Employee.Surname })).Genitive }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullName}}", MatchCase = true }, ReplaceText = (await Declensions.GetDeclensions(new List<string> { Employee.Surname, Employee.Name, Employee.Patronymic })).Instrumental }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Position}}", MatchCase = true }, ReplaceText = Employee.Position }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{ApplicationDate}}", MatchCase = true }, ReplaceText = Employee.ApplicationDate?.ToString("dd/MM/yyyy")}  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Date}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Month}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("MM") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Year}}", MatchCase = true }, ReplaceText = DateTime.Now.ToString("yy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FireIndex}}", MatchCase = true }, ReplaceText = Employee.FireIndex.ToString() }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{ContractNumber}}", MatchCase = true }, ReplaceText = Employee.ContractNumber }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Initials}}", MatchCase = true }, ReplaceText = Employee.Name.First() + "." + Employee.Surname.First() + "."} }
            };
            return new BatchUpdateDocumentRequest() { Requests = fillRequests };

        }

        public string GetFileName()
        {
            return $"Приказ об увольнении {Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetFolderName()
        {
            return $"{Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetTemplateId()
        {
            if (Employee.Company == @"ИП ""Белый КУБ""") return "1pKCHtyNcfwSVh5LaooGlkJZ2oTcLwVB8dvj16u68Op0";
            else if (Employee.Company == @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""") return "1j4KCvF7iC6y9gciVOZ32w_LEXLogZw6M1I02g0uAoBc";
            else if (Employee.Company == @"ТОО ""Мебельное ателье Белый КУБ""") return "1xGeoyLwZfdJxT43bNF0IPJyJOQuBJQKUaEu2RF1-9j8";
            else throw new ArgumentException();
        }
    }
}
