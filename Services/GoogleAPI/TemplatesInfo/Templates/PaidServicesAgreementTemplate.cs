﻿using Google.Apis.Docs.v1.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;

namespace Trello_DocumentGeneration.Services.GoogleAPI.TemplatesInfo.Templates
{
    public class PaidServicesAgreementTemplate : IDocumentTemplateFiller
    {
        public Employee Employee { get; set; }
        public PaidServicesAgreementTemplate(Employee employee)
        {
            Employee = employee;
        }
        public async Task<BatchUpdateDocumentRequest> CreateFillRequest()
        {
            Declensions fullNameDeclensions = await Declensions.GetDeclensions(new List<string> { Employee.Surname, Employee.Name, Employee.Patronymic });
            List<Request> requests = new List<Request>()
            {
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullName}}", MatchCase = true }, ReplaceText = $"{Employee.Surname} {Employee.Name} {Employee.Patronymic}" }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Position}}", MatchCase = true }, ReplaceText = Employee.Position }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{IIN}}", MatchCase = true }, ReplaceText = Employee.IIN }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Date}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{NextYearDate}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).AddYears(1).ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Month}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("MM") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Year}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("yy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{WorkIndex}}", MatchCase = true }, ReplaceText = Employee.WorkIndex.ToString() }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{PassportInfo}}", MatchCase = true }, ReplaceText = Employee.PassportInfo }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullNameTo}}", MatchCase = true }, ReplaceText = fullNameDeclensions.Dative} },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Address}}", MatchCase = true }, ReplaceText = Employee.Address?.Replace("Адрес прописки:", "")} },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{HomeAddress}}", MatchCase = true }, ReplaceText = Employee.HomeAddress } },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{PhoneNumber}}", MatchCase = true }, ReplaceText = Employee.PhoneNumber } },
            };
            return new BatchUpdateDocumentRequest() { Requests = requests };
        }

        public string GetFileName()
        {
            return $"ГПХ {Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetFolderName()
        {
            return $"{Employee.Surname} {Employee.Name} {Employee.Patronymic}";
        }

        public string GetTemplateId()
        {
            if (Employee.Company == @"ИП ""Белый КУБ""") return "1vUrNU25AJVZLqk66XoH9gMjDgGEu3caXth-4wdZqk1k";
            else if (Employee.Company == @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""") return "1N1QqpwAISdvjwliFhKXNPJFIF9kJEV9zbS-O0L-mSeU";
            else if (Employee.Company == @"ТОО ""Мебельное ателье Белый КУБ""") return "17Xz56by0zMMfa5eIQnNkybFWfBD4MMtBE_1gLH6UKzQ";
            else throw new ArgumentException();
        }
    }
}
