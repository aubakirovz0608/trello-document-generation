﻿using Google.Apis.Docs.v1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;

namespace Trello_DocumentGeneration.Services.GoogleAPI.TemplatesInfo.Templates
{
    public class ApplicationOrderTemplate : IDocumentTemplateFiller
    {
        public Employee Employee { get; set; }

        public ApplicationOrderTemplate(Employee employee)
        {
            Employee = employee;
        }
        public async Task<BatchUpdateDocumentRequest> CreateFillRequest()
        {
            List<Request> fillRequests = new List<Request>()
            {
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Surname}}", MatchCase = true }, ReplaceText = (await Declensions.GetDeclensions(new List<string> { Employee.Surname })).Genitive }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{FullName}}", MatchCase = true }, ReplaceText = (await Declensions.GetDeclensions(new List<string> { Employee.Surname, Employee.Name, Employee.Patronymic })).Accusative }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Position}}", MatchCase = true }, ReplaceText = Employee.Position }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Company}}", MatchCase = true }, ReplaceText = Employee.Company }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Date}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("dd/MM/yyyy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Month}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("MM") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Year}}", MatchCase = true }, ReplaceText = DateTime.Now.AddHours(6).ToString("yy") }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{WorkIndex}}", MatchCase = true }, ReplaceText = Employee.WorkIndex.ToString() }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{OrderIndex}}", MatchCase = true }, ReplaceText = Employee.OrderIndex.ToString() }  },
                new Request(){ ReplaceAllText = new ReplaceAllTextRequest() { ContainsText = new SubstringMatchCriteria() { Text = "{{Initials}}", MatchCase = true }, ReplaceText = Employee.Name.First() + "." + Employee.Surname.First() + "."} }
            };
            return new BatchUpdateDocumentRequest() { Requests = fillRequests };
        }

        public string GetFileName()
        {
            return $"Приказ о принятии {Employee.Surname} {Employee.Name}";
        }

        public string GetFolderName()
        {
            return $"{Employee.Surname} {Employee.Name} {Employee.Patronymic ?? ""}";
        }

        public string GetTemplateId()
        {
            if (Employee.Company == @"ИП ""Белый КУБ""") return "1Hw1X31lw1OF4uBIGc_g0Ii_q1iI7wUe3QZ_8k3CFlp0";
            else if (Employee.Company == @"ТОО ""WHITECUBE.PRO (УАЙТКУБЕ.ПРО)""") return "1l-XlA7-TDNnusuitoTFMbXNb1HbNFI99l4hqqpJIPYc";
            else if (Employee.Company == @"ТОО ""Мебельное ателье Белый КУБ""") return "1m9V0oKMpn3P0WwWVozsP7SGSXlcMSYX2vBSQUrCBPvY";
            else throw new ArgumentException();
        }
    }
}
