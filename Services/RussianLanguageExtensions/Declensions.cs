﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Trello_DocumentGeneration.Services
{
    public class Declensions
    {
        [JsonProperty("n")]
        public NumberDeclension Numbers { get; set; }
        [JsonProperty("И")]
        public string Nominative { get; set; }
        [JsonProperty("Р")]
        public string Genitive { get; set; }
        [JsonProperty("Д")]
        public string Dative { get; set; }
        [JsonProperty("В")]
        public string Accusative { get; set; }
        [JsonProperty("Т")]
        public string Instrumental { get; set; }
        [JsonProperty("П")]
        public string Prepositional { get; set; }
        public Declensions()
        {

        }
        public static async Task<Declensions> GetDeclensions(IEnumerable<string> Words)
        {
            RestClient DeclensionGetClient = new RestClient("https://ws3.morpher.ru/russian");
            RestRequest DeclensionRequest = new RestRequest("/declension");
            DeclensionRequest.AddHeader("Accept", "application/json");
            DeclensionRequest.AddQueryParameter("s", string.Join(" ", Words));
            Declensions temp = JsonConvert.DeserializeObject<Declensions>((await DeclensionGetClient.ExecuteGetAsync(DeclensionRequest)).Content);
            temp.Nominative = string.Join(" ", Words);
            return temp;
        }
        public static async Task<Declensions> GetWordsFromNumber(int userNumber)
        {
            RestClient DeclensionGetClient = new RestClient("https://ws3.morpher.ru/russian");
            RestRequest DeclensionRequest = new RestRequest($"/spell?n={userNumber}&unit=тенге");
            DeclensionRequest.AddHeader("Accept", "application/json");
            //DeclensionRequest.AddQueryParameter("n", userNumber.ToString());
            //DeclensionRequest.AddQueryParameter("unit", "тенге");
            Declensions temp = JsonConvert.DeserializeObject<Declensions>((await DeclensionGetClient.ExecuteGetAsync(DeclensionRequest)).Content);
            return temp;
        }
    }
    public class NumberDeclension
    {
        [JsonProperty("И")]
        public string Nominative { get; set; }
        [JsonProperty("Р")]
        public string Genitive { get; set; }
        [JsonProperty("Д")]
        public string Dative { get; set; }
        [JsonProperty("В")]
        public string Accusative { get; set; }
        [JsonProperty("Т")]
        public string Instrumental { get; set; }
        [JsonProperty("П")]
        public string Prepositional { get; set; }
    }
}
