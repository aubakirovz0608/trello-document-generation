﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trello_DocumentGeneration.Models;
using Trello_DocumentGeneration.Services.GoogleAPI;
using Trello_DocumentGeneration.Services.Trello;

namespace Trello_DocumentGeneration.Services.Scheduling.Jobs
{
    [DisallowConcurrentExecution]
    public class GoogleHRSpreadsheetUpdateJob : IJob
    {
        private const string SPREADSHEET_ID = "1ro2twFJDnhMOO9EIpHfT0L9-JyiKOHbV7IMAPg9e08s";

        private readonly ILogger<GoogleHRSpreadsheetUpdateJob> _logger;
        private readonly TrelloService _trelloService;
        private readonly GoogleAPIService _googleAPIService;
        public GoogleHRSpreadsheetUpdateJob(TrelloService trelloService, GoogleAPIService googleAPIService, ILogger<GoogleHRSpreadsheetUpdateJob> logger)
        {
            _logger = logger;
            _trelloService = trelloService;
            _googleAPIService = googleAPIService;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            int updatedCount = 0;
            IEnumerable<string> cardIds = await _googleAPIService.GetCardIds(SPREADSHEET_ID);
            foreach (var cardId in cardIds)
            {
                if (cardId != null && cardId.Length != 0)
                {
                    try
                    {
                        Employee employee = _trelloService.GetEmployee(cardId);
                        await _googleAPIService.UpdateRowInSpreadsheet(employee, SPREADSHEET_ID);
                        updatedCount++;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Error on updating card with id {cardId}: \n" +
                            $"{ex.Message}");
                    }
                }
            }
            _logger.LogInformation($"Updated {updatedCount} cards.");
            return;
        }
    }
}
