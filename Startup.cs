using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System.Collections.Generic;
using Trello_DocumentGeneration.Services;
using Trello_DocumentGeneration.Services.GoogleAPI;
using Trello_DocumentGeneration.Services.Scheduling;
using Trello_DocumentGeneration.Services.Scheduling.Jobs;
using Trello_DocumentGeneration.Services.Trello;

namespace Trello_DocumentGeneration
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            services.AddSingleton<GoogleAPIService>(new GoogleAPIService("credentials.json", new List<string>() { Google.Apis.Docs.v1.DocsService.Scope.Documents, Google.Apis.Docs.v1.DocsService.Scope.Drive, Google.Apis.Sheets.v4.SheetsService.Scope.Spreadsheets }, "Quickstart"));
            services.AddSingleton<WhiteCubeLastIndexes>(new WhiteCubeLastIndexes("whitecubelastindexes.json"));
            services.AddSingleton<TrelloService>(TrelloService.GetService("trellocredentials.json"));

            services.AddSingleton<GoogleHRSpreadsheetUpdateJob>();
            services.AddSingleton(new JobSchedule(typeof(GoogleHRSpreadsheetUpdateJob), "0 0 0/6 1/1 * ? *")); //every 6 hours
            services.AddHostedService<QuartzHostedService>();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(cors =>
                {
                    cors.AllowAnyHeader();
                    cors.AllowAnyOrigin();
                    cors.AllowAnyMethod();
                }
            );
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
